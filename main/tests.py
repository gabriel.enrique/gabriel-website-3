from django.test import TestCase, Client
from .models import Kegiatan, Peserta

class TestMain(TestCase):
    def test_if_home_url_exists(self):
        response = Client().get("/")
        self.assertEquals(response.status_code, 200)

    def test_if_home_page_use_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'main/home.html')
    
    def test_if_kegiatan_model_class_exists(self):
        Kegiatan.objects.create(nama="NAMA KEGIATAN")
        total = Kegiatan.objects.all().count()
        self.assertEquals(total, 1)
    
    def test_if_peserta_model_class_exists(self):
        Peserta.objects.create(nama="NAMA PESERTA")
        total = Peserta.objects.all().count()
        self.assertEquals(total, 1)

    def test_if_can_add_peserta_to_a_kegiatan(self):
        kegiatan = Kegiatan.objects.create(nama="NAMA KEGIATAN")
        peserta = Peserta.objects.create(nama="NAMA PESERTA")
        kegiatan.save()
        peserta.save()
        kegiatan.peserta.add(peserta)
        self.assertIn(peserta, Kegiatan.objects.all()[0].peserta.all())
    
    def test_if_a_peserta_can_join_many_kegiatan(self):
        kegiatan_1 = Kegiatan.objects.create(nama="NAMA KEGIATAN 1")
        kegiatan_2 = Kegiatan.objects.create(nama="NAMA KEGIATAN 2")
        kegiatan_3 = Kegiatan.objects.create(nama="NAMA KEGIATAN 3")
        peserta = Peserta.objects.create(nama="NAMA PESERTA")
        kegiatan_1.save()
        kegiatan_2.save()
        kegiatan_3.save()
        peserta.save()
        kegiatan_1.peserta.add(peserta)
        kegiatan_2.peserta.add(peserta)
        kegiatan_3.peserta.add(peserta)
        self.assertIn(kegiatan_1, peserta.kegiatans.all())
        self.assertIn(kegiatan_2, peserta.kegiatans.all())
        self.assertIn(kegiatan_3, peserta.kegiatans.all())
       
    def test_if_a_kegiatan_can_have_multiple_peserta(self):
        kegiatan = Kegiatan.objects.create(nama="NAMA KEGIATAN")
        peserta_1 = Peserta.objects.create(nama="NAMA PESERTA 1")
        peserta_2 = Peserta.objects.create(nama="NAMA PESERTA 2")
        peserta_3 = Peserta.objects.create(nama="NAMA PESERTA 3")
        kegiatan.save()
        peserta_1.save()
        peserta_2.save()
        peserta_3.save()
        kegiatan.peserta.add(peserta_1)
        kegiatan.peserta.add(peserta_2)
        kegiatan.peserta.add(peserta_3)
        self.assertIn(peserta_1, Kegiatan.objects.all()[0].peserta.all())
        self.assertIn(peserta_2, Kegiatan.objects.all()[0].peserta.all())
        self.assertIn(peserta_3, Kegiatan.objects.all()[0].peserta.all())

    def test_if_a_kegiatan_will_be_displayed_at_home_page(self):
        nama_kegiatan = "NAMA KEGIATAN"
        Kegiatan.objects.create(nama=nama_kegiatan)
        response = Client().get("/")
        page_content = response.content.decode('utf8')
        self.assertIn(nama_kegiatan, page_content)
    
    def test_if_a_peserta_that_joins_a_kegiatan_will_be_displayed_at_home_page(self):
        nama_kegiatan = "NAMA KEGIATAN"
        nama_peserta = "NAMA PESERTA"
        kegiatan = Kegiatan.objects.create(nama=nama_kegiatan)
        peserta = Peserta.objects.create(nama=nama_peserta)
        kegiatan.save()
        peserta.save()
        kegiatan.peserta.add(peserta)
        response = Client().get("/")
        page_content = response.content.decode('utf8')
        self.assertIn(nama_peserta, page_content)

    def test_if_can_have_multiple_kegiatan(self):
        nama_kegiatan_1 = "NAMA KEGIATAN 1"
        nama_kegiatan_2 = "NAMA KEGIATAN 2"
        nama_kegiatan_3 = "NAMA KEGIATAN 3"
        kegiatan_1 = Kegiatan.objects.create(nama=nama_kegiatan_1)
        kegiatan_2 = Kegiatan.objects.create(nama=nama_kegiatan_2)
        kegiatan_3 = Kegiatan.objects.create(nama=nama_kegiatan_3)
        kegiatan_1.save()
        kegiatan_2.save()
        kegiatan_3.save()
        response = Client().get("/")
        page_content = response.content.decode('utf8')
        self.assertIn(nama_kegiatan_1, page_content)
        self.assertIn(nama_kegiatan_2, page_content)
        self.assertIn(nama_kegiatan_3, page_content)

    def test_if_a_peserta_that_joins_more_than_one_kegiatan_will_be_displayed_in_all_kegiatan(self):
        nama_kegiatan_1 = "NAMA KEGIATAN 1"
        nama_kegiatan_2 = "NAMA KEGIATAN 2"
        nama_kegiatan_3 = "NAMA KEGIATAN 3"
        nama_peserta = "NAMA PESERTA"
        kegiatan_1 = Kegiatan.objects.create(nama=nama_kegiatan_1)
        kegiatan_2 = Kegiatan.objects.create(nama=nama_kegiatan_2)
        kegiatan_3 = Kegiatan.objects.create(nama=nama_kegiatan_3)
        peserta = Peserta.objects.create(nama=nama_peserta)
        kegiatan_1.save()
        kegiatan_2.save()
        kegiatan_3.save()
        peserta.save()
        kegiatan_1.peserta.add(peserta)
        kegiatan_2.peserta.add(peserta)
        kegiatan_3.peserta.add(peserta)
        response = Client().get("/")
        page_content = response.content.decode('utf8')
        self.assertEquals(page_content.count(nama_peserta), 3)

    def test_if_will_print_message_when_no_kegiatan_displayed(self):
        response = Client().get("/")
        page_content = response.content.decode('utf8')
        self.assertIn("Belum ada Kegiatan", page_content)

    def test_if_join_kegiatan_button_exists(self):
        nama_kegiatan_1 = "NAMA KEGIATAN 1"
        nama_kegiatan_2 = "NAMA KEGIATAN 2"
        nama_kegiatan_3 = "NAMA KEGIATAN 3"
        kegiatan_1 = Kegiatan.objects.create(nama=nama_kegiatan_1)
        kegiatan_2 = Kegiatan.objects.create(nama=nama_kegiatan_2)
        kegiatan_3 = Kegiatan.objects.create(nama=nama_kegiatan_3)
        kegiatan_1.save()
        kegiatan_2.save()
        kegiatan_3.save()
        response = Client().get("/")
        page_content = response.content.decode('utf8')
        self.assertIn("Mau Ikut", page_content)
        self.assertEquals(page_content.count("Mau Ikut"), 3)
    
    def test_if_join_kegiatan_button_will_go_to_that_kegiatan_join_form(self):
        nama_kegiatan = "NAMA KEGIATAN"
        kegiatan = Kegiatan.objects.create(nama=nama_kegiatan)
        kegiatan.save()
        kegiatan_id = str(kegiatan.id)
        response = Client().get(f"/ikut_kegiatan/{kegiatan_id}")
        page_content = response.content.decode('utf8')
        self.assertIn(nama_kegiatan, page_content)
        self.assertEquals(response.status_code, 200)
    
    def test_if_join_kegiatan_form_use_template(self):
        nama_kegiatan = "NAMA KEGIATAN"
        kegiatan = Kegiatan.objects.create(nama=nama_kegiatan)
        kegiatan.save()
        kegiatan_id = str(kegiatan.id)
        response = Client().get(f"/ikut_kegiatan/{kegiatan_id}")
        page_content = response.content.decode('utf8')
        self.assertIn(nama_kegiatan, page_content)
        self.assertTemplateUsed(response, 'main/join.html')
    
    def test_if_form_exists_in_join_kegiatan_page(self):
        nama_kegiatan = "NAMA KEGIATAN"
        kegiatan = Kegiatan.objects.create(nama=nama_kegiatan)
        kegiatan.save()
        kegiatan_id = str(kegiatan.id)
        response = Client().get(f"/ikut_kegiatan/{kegiatan_id}")
        page_content = response.content.decode('utf8')
        self.assertIn("<form", page_content)
        self.assertIn("</form>", page_content)

    def test_if_nama_kegiatan_will_be_displayed_when_user_visits_its_joining_page(self):
        nama_kegiatan = "NAMA KEGIATAN"
        kegiatan = Kegiatan.objects.create(nama=nama_kegiatan)
        kegiatan.save()
        kegiatan_id = str(kegiatan.id)
        response = Client().get(f"/ikut_kegiatan/{kegiatan_id}")
        page_content = response.content.decode('utf8')
        self.assertIn(nama_kegiatan, page_content)

    def test_if_add_kegiatan_button_exist(self):
        response = Client().get("/")
        page_content = response.content.decode('utf8')
        self.assertIn("Add Kegiatan", page_content)
    
    def test_if_add_kegiatan_button_directs_user_to_add_kegiatan_page(self):
        response = Client().get("/add_kegiatan")
        page_content = response.content.decode('utf8')
        self.assertEquals(response.status_code, 200)
    
    def test_if_add_kegiatan_page_use_template(self):
        response = Client().get("/add_kegiatan")
        page_content = response.content.decode('utf8')
        self.assertTemplateUsed(response, 'main/add.html')
    
    def test_if_form_exists_in_add_kegiatan_page(self):
        response = Client().get("/add_kegiatan")
        page_content = response.content.decode('utf8')
        self.assertIn("<form", page_content)
        self.assertIn("</form>", page_content)

    def test_if_csrf_token_present_in_add_kegiatan_page(self):
        response = Client().get("/add_kegiatan")
        page_content = response.content.decode('utf8')
        self.assertIn('name="csrfmiddlewaretoken"', page_content)
    
    def test_if_csrf_token_present_in_join_kegiatan_page(self):
        nama_kegiatan = "NAMA KEGIATAN"
        kegiatan = Kegiatan.objects.create(nama=nama_kegiatan)
        kegiatan.save()
        kegiatan_id = str(kegiatan.id)
        response = Client().get(f"/ikut_kegiatan/{kegiatan_id}")
        page_content = response.content.decode('utf8')
        self.assertIn('name="csrfmiddlewaretoken"', page_content)
    
    def test_if_back_button_present_in_add_kegiatan_page(self):
        response = Client().get("/add_kegiatan")
        page_content = response.content.decode('utf8')
        self.assertIn("Back", page_content)
    
    def test_if_back_button_present_in_join_kegiatan_page(self):
        nama_kegiatan = "NAMA KEGIATAN"
        kegiatan = Kegiatan.objects.create(nama=nama_kegiatan)
        kegiatan.save()
        kegiatan_id = str(kegiatan.id)
        response = Client().get(f"/ikut_kegiatan/{kegiatan_id}")
        page_content = response.content.decode('utf8')
        self.assertIn("Back", page_content)
