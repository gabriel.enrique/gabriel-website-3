from django.db import models

# Create your models here.
class Kegiatan(models.Model):
    nama = models.CharField(max_length=20)
    peserta = models.ManyToManyField("Peserta", related_name="kegiatans")

class Peserta(models.Model):
    nama = models.CharField(max_length=20)
