from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import Kegiatan, Peserta
from .forms import AddKegiatanForm, JoinKegiatanForm

def home(request):
    response = {
        "kegiatans" : Kegiatan.objects.all()
    }
    
    return render(request, "main/home.html", response)


def add_kegiatan(request):
    response = {
        "form" : AddKegiatanForm,
        "prompt" : ""
    }

    if request.method == "POST":
        form = AddKegiatanForm(request.POST or None)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect("/")
        else:
            response["prompt"] = "Maksimal 20 karakter"
            return render(request, 'main/add.html', response)

    return render(request, "main/add.html", response)


def ikut_kegiatan(request, id):
    response = {
        "nama_kegiatan" : Kegiatan.objects.get(id=id).nama,
        "form" : JoinKegiatanForm,
        "prompt" : ""
    }

    if request.method == "POST":
        form = JoinKegiatanForm(request.POST or None)
        if form.is_valid():
            peserta = form.save(commit=False)
            peserta.save()
            kegiatan = Kegiatan.objects.get(id=id)
            kegiatan.peserta.add(peserta)
            return HttpResponseRedirect("/")
        else:
            response["prompt"] = "Maksimal 20 karakter"
            return render(request, 'main/join.html', response)

    return render(request, 'main/join.html', response)
    