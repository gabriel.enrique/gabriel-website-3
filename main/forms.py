from django import forms
from .models import Kegiatan, Peserta

class JoinKegiatanForm(forms.ModelForm):
    class Meta:
        model = Peserta
        fields = ["nama"]
    
    attributes = {
        "class" : "form-control",
        "placeholder" : "Nama kamu"
    }
    
    nama = forms.CharField(label="", required=True, widget=forms.TextInput(attrs=attributes))

class AddKegiatanForm(forms.ModelForm):
    class Meta:
        model = Kegiatan
        fields = ["nama"]
    
    attributes = {
        "class" : "form-control",
        "placeholder" : "Nama kegiatan"
    }

    nama = forms.CharField(label="", required=True, widget=forms.TextInput(attrs=attributes))
    