from django.shortcuts import render

def profile(request):
    context = {}
    return render(request, "my_profile/profile.html", context)
    