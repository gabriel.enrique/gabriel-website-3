# Gabriel's Website

[![pipeline status][pipeline-badge]][commits-gl]
[![coverage report][coverage-badge]][commits-gl]

*To view this file in English, [click here][readme-en].*

Repositori ini berisi *source code* dari website yang saya buat. Website ini bisa diakses dengan cara mengunjungi [link ini][website].

[pipeline-badge]: https://gitlab.com/gabriel.enrique/gabriel-website-3/badges/master/pipeline.svg
[coverage-badge]: https://gitlab.com/gabriel.enrique/gabriel-website-3/badges/master/coverage.svg
[commits-gl]: https://gitlab.com/gabriel.enrique/gabriel-website-3/-/commits/master
[readme-en]: README.md
[website]: http://gabriel-website-3.herokuapp.com/
