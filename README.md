# Gabriel's Website

[![pipeline status][pipeline-badge]][commits-gl]
[![coverage report][coverage-badge]][commits-gl]

*Untuk melihat berkas ini dalam bahasa Indonesia, [klik di sini][readme-id].*

This repository contains the source code for my website. You can visit this website by visiting [this link][website].

[pipeline-badge]: https://gitlab.com/gabriel.enrique/gabriel-website-3/badges/master/pipeline.svg
[coverage-badge]: https://gitlab.com/gabriel.enrique/gabriel-website-3/badges/master/coverage.svg
[commits-gl]: https://gitlab.com/gabriel.enrique/gabriel-website-3/-/commits/master
[readme-id]: README.id.md
[website]: http://gabriel-website-3.herokuapp.com/
