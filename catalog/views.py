from django.shortcuts import render
from django.http import JsonResponse

import json
import random
import requests

def generate_placeholder():
    placeholders = [
        "Mau cari buku apa?",
        "Buku adalah jendela dunia...",
        "Cari buku yuk...",
        "Sudah baca buku hari ini?"
    ]
    return random.choice(placeholders)

def catalog(request):
    context = {
        'placeholder': generate_placeholder()
    }
    return render(request, "catalog/catalog.html", context)

def catalog_json(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
    api_response = requests.get(url)
    api_data = json.loads(api_response.content)
    return JsonResponse(api_data, safe=False) 
