from django.urls import path

from . import views

app_name = 'catalog'

urlpatterns = [
    path('catalog', views.catalog, name='catalog'),
    path('catalogdata', views.catalog_json)
]
