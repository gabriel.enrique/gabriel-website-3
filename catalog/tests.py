from django.test import TestCase, Client

import re

# Create your tests here.
class TestCatalog(TestCase):
    def test_if_catalog_url_exists(self):
        response = Client().get("/catalog")
        self.assertEquals(response.status_code, 200)

    def test_if_catalog_page_use_template(self):
        response = Client().get('/catalog')
        self.assertTemplateUsed(response, 'catalog/catalog.html')

    def test_if_page_title_exist(self):
        response = Client().get('/catalog')
        content = response.content.decode('utf8')
        regex_match = re.search("<h[1-9](.*)>CatalogKu<\/h[1-9]>", content)
        self.assertTrue(regex_match)

    def test_if_input_field_exist(self):
        response = Client().get('/catalog')
        content = response.content.decode('utf8')
        regex_match = re.search("<input(.*)>", content)
        self.assertTrue(regex_match)

    def test_if_input_field_has_id_user_query(self):
        response = Client().get('/catalog')
        content = response.content.decode('utf8')
        regex_match = re.search("<input(.*)id=\"user-query\"(.*)>", content)
        self.assertTrue(regex_match)

    def test_if_display_catalog_container_has_id_display_catalog(self):
        response = Client().get('/catalog')
        content = response.content.decode('utf8')
        regex_match = re.search("<div(.*)id=\"display-catalog\"(.*)>", content)
        self.assertTrue(regex_match)
