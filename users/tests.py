from django.contrib.auth.models import User
from django.test import TestCase, Client

import re

class TestUsers(TestCase):
    def test_if_register_url_exists(self):
        response = Client().get("/register")
        self.assertEquals(response.status_code, 200)

    def test_if_register_page_use_template(self):
        response = Client().get('/register')
        self.assertTemplateUsed(response, 'users/register.html')

    def test_if_registration_page_title_exist(self):
        response = Client().get('/register')
        content = response.content.decode('utf8')
        regex_match = re.search("<h[1-9](.*)>Sign Up<\/h[1-9]>", content)
        self.assertTrue(regex_match)

    def test_if_all_registration_page_input_elements_exists(self):
        response = Client().get('/register')
        content = response.content.decode('utf8')
        self.assertIn("Username", content)
        self.assertIn("Email", content)
        self.assertIn("Password", content)
        self.assertIn("Password confirmation", content)
        self.assertEquals(5, content.count("<input"))

    def test_if_sign_up_button_exists(self):
        response = Client().get('/register')
        content = response.content.decode('utf8')
        regex_match = re.search('<button(.*)type="submit"(.*)>Sign Up<\/button>', content)
        self.assertTrue(regex_match)

    def test_if_registration_guidelines_exists(self):
        response = Client().get('/register')
        content = response.content.decode('utf8')
        self.assertIn("Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.", content)
        self.assertIn("Your password can’t be too similar to your other personal information.", content)
        self.assertIn("Your password must contain at least 8 characters.", content)
        self.assertIn("Your password can’t be a commonly used password.", content)
        self.assertIn("Your password can’t be entirely numeric.", content)
        self.assertIn("Enter the same password as before, for verification.", content)

    def test_if_sign_in_shortcut_link_exists(self):
        response = Client().get('/register')
        content = response.content.decode('utf8')
        regex_match = re.search("Have An Account Already\? <a(.*)Sign In<\/a>", content)
        self.assertTrue(regex_match)

    def test_if_sign_up_process_successful(self):
        response = Client().post('/register', {
                'username': 'NoobMaster69',
                'email': 'tukangbakso@bin.gov',
                'password1': 'ieatpancakeseveryday',
                'password2': 'ieatpancakeseveryday'
            })
        user = User.objects.filter(username='NoobMaster69').first()
        self.assertIsNotNone(user)
        self.assertEquals(user.username, 'NoobMaster69')
        self.assertEquals(user.email, 'tukangbakso@bin.gov')

    def test_if_login_url_exists(self):
        response = Client().get("/login")
        self.assertEquals(response.status_code, 200)

    def test_if_login_page_use_template(self):
        response = Client().get('/login')
        self.assertTemplateUsed(response, 'users/login.html')

    def test_if_login_page_title_exist(self):
        response = Client().get('/login')
        content = response.content.decode('utf8')
        regex_match = re.search("<h[1-9](.*)>Sign In<\/h[1-9]>", content)
        self.assertTrue(regex_match)

    def test_if_all_login_page_input_elements_exists(self):
        response = Client().get('/login')
        content = response.content.decode('utf8')
        self.assertIn("Username", content)
        self.assertIn("Password", content)
        self.assertEquals(3, content.count("<input"))

    def test_if_login_button_exists(self):
        response = Client().get('/login')
        content = response.content.decode('utf8')
        regex_match = re.search('<button(.*)type="submit"(.*)>Login<\/button>', content)
        self.assertTrue(regex_match)

    def test_if_sign_up_shortcut_link_exists(self):
        response = Client().get('/login')
        content = response.content.decode('utf8')
        regex_match = re.search("Don't Have An Account\? <a(.*)Sign Up<\/a>", content)
        self.assertTrue(regex_match)

class UserLogInTest(TestCase):
    def setUp(self):
        self.credentials = {
            'username': 'NoobMaster69',
            'password': 'ieatpancakeseveryday'}
        User.objects.create_user(**self.credentials)

    def test_if_navbar_is_dynamic_depending_on_whether_or_not_a_user_is_logged_in(self):
        response = Client().get('/')
        content = response.content.decode('utf8')
        self.assertIn('<a class="nav-link" href="/login">Sign In</a>', content)
        self.assertIn('<a class="btn btn-outline-secondary" href="/register">Sign Up</a>', content)
        self.assertNotIn('Hi, <b>NoobMaster69</b>', content)
        self.assertNotIn('<a class="btn btn-outline-secondary" href="/logout">Logout</a>', content)
        response = self.client.post('/login', self.credentials, follow=True)
        content = response.content.decode('utf8')
        self.assertNotIn('<a class="nav-link" href="/login">Sign In</a>', content)
        self.assertNotIn('<a class="btn btn-outline-secondary" href="/register">Sign Up</a>', content)
        self.assertIn('Hi, <b>NoobMaster69</b>', content)
        self.assertIn('<a class="btn btn-outline-secondary" href="/logout">Logout</a>', content)
