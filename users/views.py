from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import AuthenticationForm
from django.http import HttpResponseRedirect
from django.shortcuts import render
from .forms import RegistrationForm

def register(request):
    if request.user.is_authenticated:
        messages.warning(request, "You already have an account!")
        return HttpResponseRedirect('/')
    else:
        context = {}
        
        if request.method == "POST":
            form = RegistrationForm(request.POST)
            if form.is_valid():
                form.save()
                messages.success(request, 'Your account was successfully created! You can login now')
                return HttpResponseRedirect('/login')
        else:
            form = RegistrationForm()

        context['form'] = form
        return render(request, 'users/register.html', context)

def login_view(request):
    if request.user.is_authenticated:
        messages.warning(request, "You're already logged in!")
        return HttpResponseRedirect('/')
    else:
        context = {}

        if request.method == "POST":
            form = AuthenticationForm(data=request.POST)
            if form.is_valid():
                username = form.cleaned_data['username']
                password = form.cleaned_data['password']
                user = authenticate(request, username=username, password=password)
                if user is not None:
                    login(request, user)
                    messages.success(request, f'Successfully logged in as <b>{username}</b>!', extra_tags='safe')
                    return HttpResponseRedirect('/')
        else:
            form = AuthenticationForm()

        context['form'] = form
        return render(request, 'users/login.html', context)

def logout_view(request):
    if request.user.is_authenticated:
        logout(request)
        messages.success(request, 'Successfully logged out!')
    else:
        messages.warning(request, 'You need to <a href="login" class="alert-link">login</a> in order to logout!', extra_tags='safe')
    
    return HttpResponseRedirect('/login')
