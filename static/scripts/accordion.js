$(document).ready(function() {
    // Accordion show/hide mechanics
    $(".header-text").click(function() {
        if ($(this).parent().next(".accordion-content").hasClass("active")) {
            $(this).parent().next(".accordion-content").removeClass("active").slideUp();
        } else {
            $(".accordion-section .accordion-content").removeClass("active").slideUp();
            $(this).parent().next(".accordion-content").addClass("active").slideDown();
        }
    });

    // Accordion move up mechanics
    $(".accordion-section .accordion-header .move-up").click(function() {
        var currentSection = $(this).parent().parent();
        var className = currentSection.attr("class");
        var baseClass = className.slice(0, className.length - 8);
        var orderClass = className.slice(className.length - 7, className.length);
        var order = parseInt(orderClass.split("-")[1]);

        if (order !== 1) {
            var prevSection = $(".order-" + (order - 1).toString());
            prevSection.attr("class", baseClass + " order-" + order.toString());
            currentSection.attr("class", baseClass + " order-" + (order - 1).toString());
        }
    });

    // Accordion move down mechanics
    $(".accordion-section .accordion-header .move-down").click(function() {
        var currentSection = $(this).parent().parent();
        var className = currentSection.attr("class");
        var baseClass = className.slice(0, className.length - 8);
        var orderClass = className.slice(className.length - 7, className.length);
        var order = parseInt(orderClass.split("-")[1]);

        if (order !== 4) {
            var nextSection = $(".order-" + (order + 1).toString());
            nextSection.attr("class", baseClass + " order-" + order.toString());
            currentSection.attr("class", baseClass + " order-" + (order + 1).toString());
        }
    });
});
