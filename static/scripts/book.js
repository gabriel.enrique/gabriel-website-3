$(document).ready( () => {
    /**
     * User typing countdown implementation credits to: https://stackoverflow.com/a/4220182/13294008
     * User typing countdown is implemented to reduce the amount of api calls
     */

    // Setup
    var typingTimer;                
    const doneTypingInterval = 500;  // time in ms, 0.5 second

    // On keyup, start the countdown timer
    $("#user-query").keyup( () => {
        clearTimeout(typingTimer);
        typingTimer = setTimeout(doneTyping, doneTypingInterval);
    });

    // On keydown, reset the countdown timer
    $("#user-query").keydown( () => {
        clearTimeout(typingTimer);
    });

    // When user is finished typing, this function is called
    function doneTyping() {
        const userQuery = $("#user-query").val();
        $.ajax({
            url: 'catalogdata?q=' + userQuery,
            success: (res) => {
                if (res.totalItems > 0) {
                    const books = res.items;
                    $("#display-catalog").empty();
                    for (i = 0; i < books.length; i++) {
                        const book = books[i];
                        const title = book.volumeInfo.title ? book.volumeInfo.title : '';
                        const author = book.volumeInfo.authors ? book.volumeInfo.authors.join(", ") : '';
                        const desc = book.volumeInfo.description ? book.volumeInfo.description : '';
                        const thumb = book.volumeInfo.imageLinks.thumbnail ? book.volumeInfo.imageLinks.thumbnail : '';
                        const element = ` \
                            <div class="card"> \
                                <img class="card-img-top" src="${thumb}"> \
                                <div class="card-body"> \
                                    <h5 class="card-title">${title}</h5> \
                                    <h6>${author}</h6> \
                                    <p class="card-text">${desc}</p> \
                                </div> \
                            </div>`;
                        $("#display-catalog").append(element);
                    }
                } else {
                    $("#display-catalog").empty();
                    const element = ` \
                        <div class="card"> \
                            <div class="card-body"> \
                                <h5 class="card-title">Oh tidak...</h5> \
                                <h6>Buku yang kamu cari tidak ditemukan</h6> \
                                <p class="card-text">Coba carilah buku dengan keyword yang pendek, sederhana, namun bermakna</p> \
                            </div> \
                        </div>`;
                    $("#display-catalog").append(element);
                }
            }
        });
    }
});
